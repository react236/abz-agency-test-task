export type tButton = 'yellow' | 'grey'

export interface iPosition {
    id: number
    name: string
}
