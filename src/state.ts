// import { createGlobalState } from 'react-hooks-global-state'

// const initialState = { page: 1 };
// export const { useGlobalState } = createGlobalState(initialState);

import { createStore, createHook } from 'react-sweet-state'

const Store = createStore({
    // value of the store on initialisation
    initialState: {
        page: 1,
        formSent: false
    },
    // actions that trigger store mutation
    actions: {
        setpage:
            (number) =>
                ({ setState, getState }) => {
                    // mutate state synchronously
                    setState({ page: number, })
                },
        setformsent:
            (flag) =>
                ({ setState, getState }) => {
                    // mutate state synchronously
                    setState({ formSent: flag, })
                },
    },
    // optional, mostly used for easy debugging
    name: 'testTask',
});

export const useGlobalState = createHook(Store)
