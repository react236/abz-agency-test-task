import React from 'react'
import { QueryClient, QueryClientProvider } from 'react-query'

import { TestTask } from './components/TestTask/TestTask'

const queryClient = new QueryClient()

function App() {
    return (
        <QueryClientProvider client={queryClient}>
            <TestTask />
        </QueryClientProvider>
    )
}

export default App
