import React, { useRef } from "react"
import clsx from 'clsx'
import ReactTooltip from 'react-tooltip-rc'

import { Img } from '../Image/Img'

import PhotoCover from '../../assets/photo-cover.svg'
import styles from './styles.module.scss'

export const UserCard = ({ user }) => {
    const handleMouseEnter = (e) => {
        if (e.target.offsetWidth < e.target.scrollWidth) ReactTooltip.show(e.target)
    }

    const handleMouseLeave = (e) => {
        ReactTooltip.hide(e.target)
    }

    return (
        <div className={styles.container}>
            <Img
                src={user.photo}
                alt={`${user.name} photo`}
                placeholder={<PhotoCover className={styles.image} />}
                className={styles.image}
            />
            <div
                className={clsx(styles.userName, styles.handleOverflow)}
                data-tip={user.name}
                data-event="click focus"
                onMouseEnter={handleMouseEnter}
                onMouseLeave={handleMouseLeave}
            >
                {user.name}
            </div>
            <div className={styles.userData}>
                <div className={styles.handleOverflow}>{user.position}</div>
                <div
                    className={clsx(styles.handleOverflow, styles.email)}
                    data-tip={user.email}
                    data-event="click focus"
                    onMouseEnter={handleMouseEnter}
                    onMouseLeave={handleMouseLeave}
                >
                    {user.email}
                </div>
                <div className={styles.handleOverflow}>{user.phone}</div>
            </div>
            <ReactTooltip
                place="bottom"
                type="dark"
                effect="solid"
                arrowColor={'rgba(255, 255, 255, 0'}
                offset={{top: -6, left: -20}}
                globalEventOff="click"
                className={styles.tooltip}
            />
        </div>
    )
}
