import React, { useRef } from "react"
import { useQuery, useMutation } from "react-query"
import { useFormik } from "formik"
import * as Yup from 'yup'

import { Preloader } from '../Preloader/Preloader'
import { Button } from "../Button/Button"
import { Input } from '../Input/Input'
import { PositionsSelector } from '../PositionsSelector/PositionsSelector'
import { Upload } from '../Upload/Upload'
import { getPositions } from '../../network/getPositions'

import styles from './styles.module.scss'
import { postUser } from "../../network/postUser"
import { getToken } from "../../network/getToken"
import { useGlobalState } from "../../state"

export const PostBlock = ({ refetchQuery }) => {
    const formRef = useRef(null)
    const fileRef = useRef<HTMLInputElement>()
    const [state, { setpage, setformsent }] = useGlobalState()

    const { isLoading: isPositionsLoading, isError: isPositionsError, data: positionsData } = useQuery('positions', getPositions, {
        refetchOnWindowFocus: false,
    })
    const { isLoading: isTokenLoading, isError: isTokenError, data: tokenData, refetch: tokenRefetch } = useQuery('token', getToken, {
        refetchOnWindowFocus: false,
        enabled: false,
    })

    const addUser = useMutation(data => postUser(data, tokenData.data.token))

    const handleSubmit = async (values, actions) => {
        const newValues = Object.assign({}, values)
        // Получаем ID позиции из названия
        const position_id = positionsData.data.positions.find( item => item.name === newValues.position).id
        newValues.position_id = position_id
        delete newValues.position

        const file = fileRef.current.files[0]
        newValues.photo = file

        try {
            await tokenRefetch()
            await addUser.mutateAsync(newValues)
            setpage(1)
            setformsent(true)
            await refetchQuery()
            actions.resetForm()
        } catch (e) {
            console.log(e);
        }
    }

    const validationSchema = () =>
        Yup.object().shape({
            name: Yup.string()
                .min(2, 'Too short')
                .max(60, 'Too long')
                .required('Required field'),
            email: Yup.string()
                .min(2, 'Too short')
                .max(50, 'Too long')
                .email('Must be valid email')
                .required('Required field'),
            phone: Yup.string()
                .test(
                    'is-phone-number',
                    'Incorrect phone number',
                    //(value) => /\+38 ?\(?0[0-9]{2}\)? ?[0-9]{3}-?[0-9]{2}-?[0-9]{2}/.test(value)
                    value => /[\+]{0,1}380([0-9]{9})/.test(value)
                )
                .required('Required field'),
            position: Yup.string()
                .required('Required field'),
            photo: Yup.string()
                .required('Required field'),
        })

    const formik = useFormik({
        initialValues: {
            name: '',
            email: '',
            phone: '',
            position: '',
            photo: '',
        },
        validationSchema,
        onSubmit: handleSubmit,
    })
    
    const handleSubmitClick = () => {
        formik.handleSubmit()
    }

    return (
        <div id="postblock" className={styles.container}>
            <h1>Working with POST request</h1>
            <form onSubmit={formik.handleSubmit} ref={formRef}>
                <div className={styles.formBlock}>
                    <Input
                        id='name'
                        name='name'
                        value={formik.values.name}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        type="text"
                        placeholder="Your name"
                        error={formik.errors.name}
                        touched={formik.touched.name}
                    />
                    <Input
                        id='email'
                        name='email'
                        value={formik.values.email}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        type="email"
                        placeholder="Email"
                        className={styles.inputMargin}
                        error={formik.errors.email}
                        touched={formik.touched.email}
                    />
                    <Input
                        id='phone'
                        name='phone'
                        value={formik.values.phone}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        type="phone"
                        placeholder="Phone"
                        className={styles.inputMargin}
                        error={formik.errors.phone}
                        touched={formik.touched.phone}
                        phone
                    />
                    <div className={styles.phoneTemplate}>+38 (XXX) XXX - XX - XX</div>
                    <div className={styles.positionSelector}>
                        <div>Select Your Position</div>
                        <div className={styles.radioButtonsBlock}>
                            {(isPositionsLoading || isTokenLoading || addUser.isLoading) && <Preloader className={styles.spinner} />}
                            {!isPositionsLoading && !isPositionsError && (
                                <PositionsSelector
                                    positions={positionsData.data.positions}
                                    name='position'
                                    value={formik.values.position}
                                    onChange={formik.handleChange}
                                    onBlur={formik.handleBlur}
                                    error={formik.errors.position}
                                    touched={formik.touched.position}
                                />
                            )}
                        </div>
                    </div>
                    <Upload
                        id='photo'
                        name='photo'
                        value={formik.values.photo}
                        onChange={formik.handleChange}
                        onBlur={formik.handleBlur}
                        placeholder="Upload your photo"
                        className={styles.inputMargin}
                        error={formik.errors.photo}
                        touched={formik.touched.photo}
                        ref={fileRef}
                    />
                </div>
                <div className={styles.buttonContainer}>
                    <Button
                        type={formik.isValid && Object.entries(formik.touched).length>0 ? "yellow" : "grey"}
                        text="Sign Up"
                        onClick={handleSubmitClick}
                    />
                </div>
            </form>
        </div>
    )
}
