import React, { FocusEvent } from "react"
import { nanoid } from 'nanoid'

import { iPosition } from "../../types"
import styles from './styles.module.scss'

interface iProps {
    positions: iPosition[]
    name: string
    value: string
    onChange: (number) => void
    onBlur: (e: FocusEvent<any, Element>) => void
    error: any
    touched: any
}

export const PositionsSelector: React.FC<iProps> = ({ positions, name, value, onChange, onBlur, error, touched }) => {
    const handleChange = (e) => {
        onChange(e)
    }

    return (
        <div className={styles.container}>
            { positions.map( (item) => {
                const id = nanoid(5)
                
                return (
                    <div key={item.id} className={styles.positionItem}>
                        
                        <input
                            type="radio"
                            id={`${id}-${item.id}`}
                            name={name}
                            value={item.name}
                            className={styles.positionInput}
                            onChange={handleChange}
                            checked={value === item.name}
                        />
                        {value === item.name && <div className={styles.positionInputChecked}></div>}
                        <label className={styles.positionLabel} htmlFor={item.name}>{item.name}</label>
                    </div>
                )
            })}
            {error && <div className={styles.errorText}>{error}</div>}
        </div>
    )
}
