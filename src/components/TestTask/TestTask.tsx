import React from 'react'
import { useQuery } from "react-query"

import { Header } from '../Header/Header'
import { Intro } from '../Intro/Intro'
import { GetBlock } from '../GetBlock/GetBlock'
import { PostBlock } from '../PostBlock/PostBlock'
import { Success } from '../Success/Success'
import { getUsers } from '../../network/getUsers'
import { useGlobalState } from "../../state"

import styles from './styles.module.scss'

export const TestTask = () => {
    const [state] = useGlobalState()

    const usersQuery = useQuery(['users', state.page], () => getUsers(state.page, 6), {
        refetchOnWindowFocus: false,
        keepPreviousData: true,
    })

    return (
        <div className={styles.mainContainer}>
            <Header />
            <div className={styles.container}>
                <Intro />
                <GetBlock queryData={usersQuery} />
                {!state.formSent
                    ? <PostBlock refetchQuery={usersQuery.refetch} />
                    : <Success />
                }
            </div>
        </div>
    )
}
