import React from "react"

import { Button } from "../Button/Button"

import background from '../../assets/pexels-alexandr-podvalny-1227513.webp'
import styles from './styles.module.scss'

export const Intro = () => {
    const handleClick = () => {
        const elem = document.getElementById('postblock')
        window.scrollTo(0, elem.offsetTop-20)
    }

    return (
        <div className={styles.container}>
            <div className={styles.image} />
            <div className={styles.imageText}>
                <h1 className={styles.imageHeader}>Test assignment for front-end developer</h1>
                <div className={styles.description}>What defines a good front-end developer is one that has skilled knowledge of HTML, CSS, JS with a vast understanding of User design thinking as they'll be building web interfaces with accessibility in mind. They should also be excited to learn, as the world of Front-End Development keeps evolving.</div>
                <div className={styles.buttonContainer}>
                    <Button type="yellow" text="Sign Up" onClick={handleClick} />
                </div>
            </div>
        </div>
    )
}
