/**
 * Компонент проверки доступности изображений.
 * Выводит заглушку (svg), если картинка еще не загружена или недоступна.
 * Когда картинка становится доступной, выводит ее.
 */

import React, { useState, useEffect } from 'react'

interface iProps {
    src: string
    alt: string
    placeholder: any
    className: string
}

export const Img: React.FC<iProps> = ({ src, alt, placeholder, className = '' }) => {
    const [imageReady, setImageReady] = useState(false)

    useEffect(() => {
        // Проверяем успешно ли загружается картинка
        const image = new Image()

        image.src = src

        image.onload = () => {
            setImageReady(true)
        }
    }, [])

    return (
        <>
            {imageReady
                ? src
                    ? <img src={src} alt={alt} className={className} />
                    : placeholder
                : placeholder
            }
        </>
    )
}

