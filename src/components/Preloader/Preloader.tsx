import React from "react"
import { ColorRing } from 'react-loader-spinner'

interface iProps {
    className: string
}

export const Preloader: React.FC<iProps> = ({ className }) => {
    return (
        <ColorRing 
            visible={true}
            height="48"
            width="48"
            ariaLabel="blocks-loading"
            wrapperStyle={{}}
            wrapperClass={`blocks-wrapper ${className}`}
            colors={['#00BDD3', '#00BDD3', '#00BDD3', '#00BDD3', '#00BDD3']}
        />
    )
}
