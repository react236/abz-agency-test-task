import React from "react"

import { Preloader } from '../Preloader/Preloader'
import { UserCard } from '../UserCard/UserCard'
import { Button } from "../Button/Button"
import { useGlobalState } from "../../state"

import styles from './styles.module.scss'

export const GetBlock = ({ queryData }) => {
    const [state, { setpage }] = useGlobalState()

    const { isLoading, isFetching, isError, data: users } = queryData

    const handleClickMore = () => {
        const newPage = state.page+1
        if (state.page < users.data.total_pages) {
            setpage(newPage)
        }
    }

    return (
        <div id="usersblock" className={styles.container}>
            <h1>Working with GET request</h1>
            <div className={styles.usersBlock}>
                {isFetching && <Preloader className={styles.spinner} />}
                {!isLoading && !isError && users.data.users.map((item) => (
                    <UserCard key={item.id} user={item} />
                ))}
            </div>
            {!isLoading && !isError && state.page < users.data.total_pages && (
                <div className={styles.buttonContainer}>
                    <Button type="yellow" text="Show More" width={120} onClick={handleClickMore} />
                </div>
            )}
        </div>
    )
}
