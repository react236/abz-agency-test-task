import React from "react"
import clsx from "clsx"

import { tButton } from '../../types'
import styles from './styles.module.scss'

interface iProps {
    type: tButton
    text: string
    width?: number
    onClick?: () => void
}

const getClass = (type: tButton) => {
    switch (type) {
        case 'yellow': return styles.yellow
        case 'grey': return styles.grey
        default: return ''
    }
}

export const Button: React.FC<iProps> = ({ type, text = '', width = 100, onClick }) => {
    const handleClick = (e) => {
        if (type !== 'grey') onClick()
    }

    const buttonClass = getClass(type)

    return (
        <div
            className={clsx(styles.container, buttonClass)}
            style={{ width: `${width}px` }}
            onClick={handleClick}
        >
            <span>{text}</span>
        </div>
    )
}
