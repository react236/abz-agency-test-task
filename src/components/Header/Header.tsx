import React from "react"

import { Button } from '../Button/Button'

import Logo from '../../assets/Logo.svg'
import styles from './styles.module.scss'

export const Header = () => {
    const handleSighInClick = () => {
        const elem = document.getElementById('postblock')
        window.scrollTo(0, elem.offsetTop-20)
    }

    const handleUsersClick = () => {
        const elem = document.getElementById('usersblock')
        window.scrollTo(0, elem.offsetTop-20)
    }

    return (
        <div className={styles.container}>
            <div className={styles.innerContainer}>
                <Logo />
                <div className={styles.buttonsContainer}>
                    <Button type="yellow" text="Users" onClick={handleUsersClick} />
                    <Button type="yellow" text="Sign Up" onClick={handleSighInClick} />
                </div>
            </div>
        </div>
    )
}
