import React, { FocusEvent, useState, useEffect } from 'react'
import clsx from 'clsx'

import styles from './styles.module.scss'

interface iProps {
    id: string
    name: string
    value: string
    onChange: (string) => void
    onBlur: (e: FocusEvent<any, Element>) => void
    placeholder: string
    className?: string
    error: any
    touched: any
}

export const Upload = React.forwardRef<HTMLInputElement, iProps>( 
    ({ id, name, value, onChange, onBlur, className, placeholder, error, touched }, ref) => {
        const [fileSelected, setFileSelected] = useState(false)

        const handleClick = (e) => {
            const elem = document.getElementById(id)
            if (elem) elem.click()
        }

        const handleChange = (e) => {
            if (e.target.value) setFileSelected(true)
            onChange(e)
        }

        // При сбросе формы после отправки нужно сбросить флаг заполненности
        useEffect(() => {
            if (value === '') setFileSelected(false)
        }, [value])

        return (
            <div className={styles.container}>
                <input
                    id={id}
                    name={name}
                    value={value}
                    type="file"
                    className={styles.uploadInputElement}
                    placeholder={placeholder}
                    onChange={handleChange}
                    accept="image/jpeg"
                    ref={ref}
                />
                <div className={styles.uploadForm}>
                    <div className={ clsx(styles.uploadButton, {[styles.uploadButtonError]: touched && error}) } onClick={handleClick} onBlur={onBlur}>Upload</div>
                    {fileSelected ? (
                        <div className={ clsx(styles.uploadField, styles.fileSelected, {[styles.uploadFieldError]: touched && error}) }>{value}</div>
                    ) : (
                        <div className={ clsx(styles.uploadField, {[styles.uploadFieldError]: touched && error}) }>{placeholder}</div>
                    )}
                </div>
                {touched && error && <div className={styles.errorText}>{error}</div>}
            </div>
        )
    }
)
