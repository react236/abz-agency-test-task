import React from "react"

import SuccessImage from '../../assets/success-image.svg'
import styles from './styles.module.scss'

export const Success = () => {
    return (
        <div className={styles.container}>
            <h1>User successfully registered</h1>
            <div className={styles.image}>
                <SuccessImage />
            </div>
        </div>
    )
}
