import React, { useState, FocusEvent, useEffect } from 'react'
import clsx from 'clsx'
import { FormikErrors, FormikTouched } from 'formik'

import styles from './styles.module.scss'

interface iProps {
    id: string
    name: string
    value: string
    onChange: (string) => void
    onBlur: (e: FocusEvent<any, Element>) => void
    type: string
    placeholder: string
    className?: string
    error: any
    touched: any
    phone?: boolean
}

export const Input: React.FC<iProps> = ({ id, name, value, onChange, onBlur, type='', placeholder, className='', error, touched, phone=false }) => {
    const [filled, setFilled] = useState<boolean>(false)
    
    const handleFocus = (e) => {
        setFilled(true)
    }

    const handleBlur = (e) => {
        if (value === '') setFilled(false)
        onBlur(e)
    }

    useEffect(() => {
        // При сбросе формы после отправки нужно сбросить флаг заполненности
        if (value === '') setFilled(false)
    }, [value])

    return (
        <div className={styles.container}>
            <input
                id={id}
                name={name}
                value={value}
                type={type}
                className={clsx(styles.input, className, {[styles.inputError]: touched && error})}
                onChange={onChange}
                onFocus={handleFocus}
                onBlur={handleBlur}
            />
            <label
                htmlFor={id}
                className={ clsx(
                    styles.label,
                    {
                        [styles.labelUp]: filled,
                        [styles.labelError]: touched && error
                    }
                )}>
                    {placeholder}
                </label>
            {touched && error && <div className={ clsx(styles.errorText, {[styles.errorTextPhone]: phone}) }>{error}</div>}
        </div>
    )
}
