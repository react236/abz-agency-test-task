import axios from "axios"

import { SRV } from '../constants'

export const getUser = async (id) => {
    try {
        const result = await axios.get(`${SRV}/user/${id}`)
        return result
    } catch (e) {
        console.log(e);
        
    }
}
