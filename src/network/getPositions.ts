import axios from "axios"

import { SRV } from '../constants'

export const getPositions = async () => {
    try {
        const result = await axios.get(`${SRV}/positions`)
        return result
    } catch (e) {
        console.log(e);
        
    }
}
