import axios from "axios"

import { SRV } from '../constants'

export const getUsers = async (page: number, count: number) => {
    try {
        const result = await axios.get(`${SRV}/users?page=${page}&count=${count}`)
        return result
    } catch (e) {
        throw (e)
    }
}
