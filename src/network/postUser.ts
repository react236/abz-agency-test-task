import axios from "axios"

import { SRV } from '../constants'

export const postUser = async (user, token) => {
    const headers = {
        'Token': token,
        'Content-Type': 'multipart/form-data',
    }

    try {
        const result = await axios.post(`${SRV}/users`, user, { headers })
        return result
    } catch (e) {
        throw (e)
    }
}
