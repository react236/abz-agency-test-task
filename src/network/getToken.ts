import axios from "axios"

import { SRV } from '../constants'

export const getToken = async () => {
    try {
        const result = await axios.get(`${SRV}/token`)
        return result
    } catch (e) {
        console.log(e);
        
    }
}
